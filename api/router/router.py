from flask_restplus import Api
from views import auth, test
# from views import detect
# from views.dokum import single, bulk
from views.dokum import single_v2 as single
from views.dokum import bulk_v2 as bulk
from exceptions import error_handler
from utils import output_utils

from flask import Blueprint


bp = Blueprint('Dokum', __name__)
#api = Api(bp, doc=False)
api = Api(bp, doc='/docs/')


# Namespace
# api.add_namespace(auth.api, path='/api/v1/auth')
# api.add_namespace(test.api, path='/api/v1/test')
api.add_namespace(single.api, path='/api/v1/dokum/single')
api.add_namespace(bulk.api, path='/api/v1/dokum/bulk')
# api.add_namespace(detect.api, path='/api/v1/detect/language')

# Resource Map
# auth.api.add_resource(auth.Login, '/login')
# test.api.add_resource(test.Test, '')
single.api.add_resource(single.SinglePlain, '/plain')
single.api.add_resource(single.SingleSplitKorean, '/split/korean')
bulk.api.add_resource(bulk.BulkPlain, '/plain')
bulk.api.add_resource(bulk.BulkSplitKorean, '/split/korean')
# detect.api.add_resource(detect.Detect, '')


@api.errorhandler(error_handler.NotFoundError)
@api.errorhandler(error_handler.NotAuthorizedError)
@api.errorhandler(error_handler.PermissionError)
def handle_standard(error):
    return output_utils.response_standard(
        start_time=error.start_time,
        code=error.code,
        operation=error.operation,
        message=error.message,
        extra=error.extra
    )


@api.errorhandler(error_handler.DefaultError)
@api.errorhandler(error_handler.ValidationError)
def handle_warning(error):
    return output_utils.response_warning(
        start_time=error.start_time,
        code=error.code,
        error=error.error,
        operation=error.operation,
        message=error.message,
        extra=error.extra
    )


@api.errorhandler(error_handler.ServerError)
def handle_error(error):
    return output_utils.response_error(
        start_time=error.start_time,
        code=error.code,
        error=error.error,
        operation=error.operation,
        message=error.message,
        extra=error.extra
    )
