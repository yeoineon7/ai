import time
from flask import current_app as app
from flask_restplus import Resource, Namespace

from auth.jwt_token import encode_jwt
from exceptions import error_handler
from utils.output_utils import response_standard


api = Namespace('Auth', description='Auth API')

parser = api.parser()
parser.add_argument('id', type=str, help='Input Your ID', required=True)
parser.add_argument('pw', type=str, help='Input Your PW', required=True)


@api.doc(parser=parser)
class Login(Resource):
    def post(self, **kwargs):
        start_time = time.perf_counter()
        args = parser.parse_args()
        try:
            id = args['id']
            pw = args['pw']

            # need to id, pw valid check

        except Exception as e:
            raise error_handler.ValidationError(
                operation='Auth_Login', error=str(e), start_time=start_time)

        else:
            res, code = response_standard(
                start_time, operation='Auth_Login', message='token is generated')
            res['access_token'] = str(encode_jwt())
            return res, code
