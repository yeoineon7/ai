from flask import current_app as app
from flask_restplus import Resource, Namespace
from auth.jwt_token import jwt_token_required
from exceptions import error_handler
from utils.output_utils import response_standard

import time


api = Namespace('Test', description='Test API')

parser = api.parser()
parser.add_argument('Authorization', help='Auth Key', location='headers',
                    required=True)
parser.add_argument('test_code', type=int,
                    help='1: Success, 2: DefaultError(code=505), 3: NotFoundError, 4: NotAuthorizedError, 5: ValidationError, 6: PermissionError, 7: ServerError', required=True)


@api.doc(parser=parser)
class Test(Resource):
    @jwt_token_required
    def post(self, **kwargs):
        start_time = time.perf_counter()
        token = kwargs['decoded_token']
        if 'dokum' in token['scope']:
            args = parser.parse_args()

            test_code = int(args['test_code'])
            if test_code is 1:
                return response_standard(start_time, operation='Test_API', message='Test success')
            elif test_code is 2:
                raise error_handler.DefaultError(
                    code=505, operation='Test_API', start_time=start_time)
            elif test_code is 3:
                raise error_handler.NotFoundError(
                    operation='Test_API', start_time=start_time)
            elif test_code is 4:
                raise error_handler.NotAuthorizedError(
                    operation='Test_API', start_time=start_time)
            elif test_code is 5:
                raise error_handler.ValidationError(
                    operation='Test_API', start_time=start_time)
            elif test_code is 6:
                raise error_handler.PermissionError(
                    operation='Test_API', start_time=start_time)
            elif test_code is 7:
                raise error_handler.ServerError(
                    operation='Test_API', start_time=start_time)
            else:
                raise error_handler.ValidationError(
                    operation='Test_API', start_time=start_time)
        else:
            raise error_handler.NotAuthorizedError(
                operation='Test_API', start_time=start_time)
