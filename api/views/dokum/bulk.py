from flask_restplus import Resource, Namespace, inputs
from nmt_custom.connector.model_holder import get_model_instance, transliteration
from utils import dokum_utils as du
from auth.jwt_token import jwt_token_required
from exceptions import error_handler
from utils.output_utils import response_standard
from utils.cleaning_utils import rm_space
import time


api = Namespace('Dokum', description='Dokum API')

parser = api.parser()
# parser.add_argument('Authorization', help='Auth Key', location='headers',
#                    required=True)
parser.add_argument('input_texts', type=str, action='append', help='Input Korean or English sentences by List (MAX 1000 Words)',
                    required=True)
parser.add_argument('count', type=int, default=2, help='The number of output Sentences',
                    required=False)
parser.add_argument('preprocess', type=inputs.boolean, default=True, help='Please Set True If you want preprocessing',
                    required=True)


@api.doc(parser=parser)
class BulkPlain(Resource):
    # @jwt_token_required
    def post(self, **kwargs):
        start_time = time.perf_counter()
        args = parser.parse_args()

        input_texts = args['input_texts']
        count = args['count']
        preprocess = args['preprocess']

        pure_input_texts = input_texts
        num_translations_per_input = count

        if len(input_texts) > 1000:
            raise error_handler.ValidationError(operation='Bulk_Plain', start_time=start_time,
                                                message='Max 1000 Words (%d)' % len(input_texts))

        if preprocess:
            input_texts = list(map(du.remove_special_symbol, input_texts))
            input_texts = list(map(du.remove_not_support_char, input_texts))
        input_texts = [input_text.lower() for input_text in input_texts]

        if ('' in list(map(rm_space, input_texts))) or (None in input_texts):
            raise error_handler.ValidationError(operation='Bulk_Plain', start_time=start_time,
                                                message='Empty input in List')

        try:
            mh = get_model_instance()
            result, num_translations_per_input = transliteration(
                mh, input_texts, count)
        except Exception as e:
            raise error_handler.ServerError(
                operation='Bulk_Plain', start_time=start_time, error=str(e))

        num_of_result_arr = list()
        for _input, _output in zip(pure_input_texts, result):
            num_of_result_arr.append({
                'input_text': _input,
                'output': _output,
                'count': num_translations_per_input
            })

        extra = {
            'num_of_inputs': len(pure_input_texts),
            'num_of_result': len(num_of_result_arr)
        }

        res, code = response_standard(
            start_time, operation='Bulk_Plain', extra=extra)
        res['num_of_inputs'] = extra['num_of_inputs']
        res['result'] = num_of_result_arr
        return res, code


@api.doc(parser=parser)
class BulkSplitKorean(Resource):
    # @jwt_token_required
    def post(self, **kwargs):
        start_time = time.perf_counter()
        args = parser.parse_args()

        input_texts = args['input_texts']
        count = args['count']
        preprocess = args['preprocess']

        pure_input_texts = input_texts
        num_translations_per_input = count
        response = list()

        if len(input_texts) > 1000:
            raise error_handler.ValidationError(operation='Bulk_Split_Korean', start_time=start_time,
                                                message='Max 1000 Words (%d)' % len(input_texts))

        if preprocess:
            input_texts = list(map(du.remove_special_symbol, input_texts))
            input_texts = list(map(du.remove_not_support_char, input_texts))
        input_texts = [input_text.lower() for input_text in input_texts]

        if ('' in list(map(rm_space, input_texts))) or (None in input_texts):
            raise error_handler.ValidationError(operation='Bulk_Split_Korean', start_time=start_time,
                                                message='Empty input in List')

        try:
            mh = get_model_instance()
        except Exception as e:
            raise error_handler.ServerError(operation='Bulk_Split_Korean', start_time=start_time, error=str(e),
                                            message='Model Instance Load Error', extra={'num_of_inputs': len(pure_input_texts)})

        for idx, input_text in enumerate(input_texts):
            input_text_arr = du.detach_kor(input_text)
            if len(input_text_arr) > 5:
                response.append({
                    'input_text': pure_input_texts[idx],
                    'output': None,
                    'message': 'It takes too long to calculate the Probability, ( Seperate counts : %d )' % len(input_text_arr),
                    'count': num_translations_per_input
                })
            else:
                result_arr = list()
                try:
                    for item in input_text_arr:
                        input_text = item['text']
                        if item['language'] == 'en':
                            input_text, num_translations_per_input = transliteration(
                                mh, [input_text], count)
                            result_arr.append(input_text[0])
                        else:
                            result_arr.append(input_text)

                    response.append({
                        'input_text': pure_input_texts[idx],
                        'output': du.join_by_probs(result_arr, num_translations_per_input)[:num_translations_per_input],
                        'count': num_translations_per_input
                    })
                except Exception as e:
                    raise error_handler.ServerError(operation='Bulk_Split_Korean', start_time=start_time, error=str(e),
                                                    extra={'input_text': pure_input_texts[idx]})

        extra = {
            'num_of_inputs': len(pure_input_texts),
            'num_of_result': len(response)
        }

        res, code = response_standard(
            start_time, operation='Bulk_Split_Korean', extra=extra)
        res['num_of_inputs'] = extra['num_of_inputs']
        res['result'] = response
        return res, code


# 191202
# @api.doc(parser=parser)
# class BulkPlain(Resource):
#   #@jwt_token_required
#   def post(self, **kwargs):
#     start_time = time.perf_counter()
#     token = kwargs['decoded_token']
#     if 'dokum' in token['scope']:
#       args = parser.parse_args()
#
#       input_texts = args['input_texts']
#       count = args['count']
#       preprocess = args['preprocess']
#
#       pure_input_texts = input_texts
#       num_translations_per_input = count
#
#       if len(input_texts) > 1000:
#         raise error_handler.ValidationError(operation='Bulk_Plain', start_time=start_time,
#             message='Max 1000 Words (%d)' % len(input_texts))
#
#       if preprocess:
#         input_texts = list(map(du.remove_special_symbol, input_texts))
#         input_texts = list(map(du.remove_not_support_char, input_texts))
#       input_texts = [input_text.lower() for input_text in input_texts]
#
#       if ('' in list(map(rm_space, input_texts))) or (None in input_texts):
#         raise error_handler.ValidationError(operation='Bulk_Plain', start_time=start_time,
#             message='Empty input in List')
#
#       try:
#         mh = get_model_instance()
#         result, num_translations_per_input = transliteration(mh, input_texts, count)
#       except Exception as e:
#         raise error_handler.ServerError(operation='Bulk_Plain', start_time=start_time, error=str(e))
#
#       num_of_result_arr = list()
#       for _input, _output in zip(pure_input_texts, result):
#         num_of_result_arr.append({
#           'input_text': _input,
#           'output': _output,
#           'count': num_translations_per_input
#         })
#
#       extra = {
#         'num_of_inputs': len(pure_input_texts),
#         'num_of_result': len(num_of_result_arr)
#       }
#
#       res, code =  response_standard(start_time, operation='Bulk_Plain', extra=extra)
#       res['num_of_inputs'] = extra['num_of_inputs']
#       res['result'] = num_of_result_arr
#       return res, code
#
#     else:
#       raise error_handler.NotAuthorizedError(operation='Bulk_Plain', start_time=start_time)
#
# @api.doc(parser=parser)
# class BulkSplitKorean(Resource):
#   #@jwt_token_required
#   def post(self, **kwargs):
#     start_time = time.perf_counter()
#     token = kwargs['decoded_token']
#     if 'dokum' in token['scope']:
#       args = parser.parse_args()
#
#       input_texts = args['input_texts']
#       count = args['count']
#       preprocess = args['preprocess']
#
#       pure_input_texts = input_texts
#       num_translations_per_input = count
#       response = list()
#
#       if len(input_texts) > 1000:
#         raise error_handler.ValidationError(operation='Bulk_Split_Korean', start_time=start_time,
#             message='Max 1000 Words (%d)' % len(input_texts))
#
#       if preprocess:
#         input_texts = list(map(du.remove_special_symbol, input_texts))
#         input_texts = list(map(du.remove_not_support_char, input_texts))
#       input_texts = [input_text.lower() for input_text in input_texts]
#
#       if ('' in list(map(rm_space, input_texts))) or (None in input_texts):
#         raise error_handler.ValidationError(operation='Bulk_Split_Korean', start_time=start_time,
#             message='Empty input in List')
#
#       try:
#         mh = get_model_instance()
#       except Exception as e:
#         raise error_handler.ServerError(operation='Bulk_Split_Korean', start_time=start_time, error=str(e),
#               message='Model Instance Load Error', extra={'num_of_inputs': len(pure_input_texts)})
#
#       for idx, input_text in enumerate(input_texts):
#         input_text_arr = du.detach_kor(input_text)
#         if len(input_text_arr) > 5:
#           response.append({
#             'input_text': pure_input_texts[idx],
#             'output': None,
#             'message': 'It takes too long to calculate the Probability, ( Seperate counts : %d )' % len(input_text_arr),
#             'count': num_translations_per_input
#           })
#         else:
#           result_arr = list()
#           try:
#             for item in input_text_arr:
#               input_text = item['text']
#               if item['language'] == 'en':
#                 input_text, num_translations_per_input = transliteration(mh, [input_text], count)
#                 result_arr.append(input_text[0])
#               else:
#                 result_arr.append(input_text)
#
#             response.append({
#               'input_text': pure_input_texts[idx],
#               'output': du.join_by_probs(result_arr, num_translations_per_input)[:num_translations_per_input],
#               'count': num_translations_per_input
#             })
#           except Exception as e:
#             raise error_handler.ServerError(operation='Bulk_Split_Korean', start_time=start_time, error=str(e),
#               extra={'input_text': pure_input_texts[idx]})
#
#       extra = {
#         'num_of_inputs': len(pure_input_texts),
#         'num_of_result': len(response)
#       }
#
#       res, code =  response_standard(start_time, operation='Bulk_Split_Korean', extra=extra)
#       res['num_of_inputs'] = extra['num_of_inputs']
#       res['result'] = response
#       return res, code
#
#     else:
#       raise error_handler.NotAuthorizedError(operation='Bulk_Split_Korean', start_time=start_time)
