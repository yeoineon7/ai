from flask_restplus import Resource, Namespace, inputs
from nmt_custom.connector.model_holder import get_model_instance, transliteration
from utils import dokum_utils as du
from auth.jwt_token import jwt_token_required
from exceptions import error_handler
from utils.output_utils import response_standard
from utils.cleaning_utils import rm_space

import time

api = Namespace('Dokum', description='Dokum API')

parser = api.parser()
# parser.add_argument('Authorization', help='Auth Key', location='headers',
#                    required=True)
parser.add_argument('input_text', type=str, help='Input a Korean or English sentence',
                    location='form',
                    required=True)
parser.add_argument('count', type=int, default=2, help='The number of output Sentences',
                    location='form',
                    required=False)
parser.add_argument('preprocess', type=inputs.boolean, default=True, help='Please Set True If you want preprocessing',
                    location='json',
                    required=True)


@api.doc(parser=parser)
class SinglePlain(Resource):
    # @jwt_token_required
    def post(self, **kwargs):
        start_time = time.perf_counter()
        args = parser.parse_args()

        input_text = args['input_text']
        count = args['count']
        preprocess = args['preprocess']

        pure_input_text = input_text
        num_translations_per_input = count
        result = None

        if preprocess:
            input_text = du.remove_special_symbol(input_text)
            input_text = du.remove_not_support_char(input_text)

        input_text = input_text.lower()
        if rm_space(input_text) == '' or input_text == None:
            raise error_handler.ValidationError(operation='Single_Plain', start_time=start_time,
                                                message='input is Empty')
        else:
            try:
                mh = get_model_instance()
                result, num_translations_per_input = transliteration(
                    mh, [input_text], count)
            except Exception as e:
                raise error_handler.ServerError(
                    operation='Single_Plain', start_time=start_time, error=str(e))

        extra = {
            'input_text': pure_input_text,
            'output': result[0],
            'count': num_translations_per_input
        }
        res, code = response_standard(
            start_time, operation='Single_Plain', extra=extra)
        res['input_text'] = extra['input_text']
        res['output'] = extra['output']
        res['count'] = extra['count']
        return res, code


@api.doc(parser=parser)
class SingleSplitKorean(Resource):
    # @jwt_token_required
    def post(self, **kwargs):
        start_time = time.perf_counter()
        args = parser.parse_args()

        input_text = args['input_text']
        count = args['count']
        preprocess = args['preprocess']

        pure_input_text = input_text
        num_translations_per_input = count
        result = None

        if preprocess:
            input_text = du.remove_special_symbol(input_text)
            input_text = du.remove_not_support_char(input_text)

        input_text = input_text.lower()
        if rm_space(input_text) == '' or input_text == None:
            raise error_handler.ValidationError(operation='Single_Split_Korean', start_time=start_time,
                                                message='input is Empty')
        else:
            input_text_arr = du.detach_kor(input_text)
            if len(input_text_arr) > 5:
                raise error_handler.ValidationError(operation='Single_Split_Korean', start_time=start_time,
                                                    message='It takes too long to calculate the Probability, ( Seperate counts : %d )' % len(
                                                        input_text_arr),
                                                    extra={'input_text': pure_input_text})

            try:
                mh = get_model_instance()
                result_arr = list()
                for item in input_text_arr:
                    input_text = item['text']
                    if item['language'] == 'en':
                        input_text, num_translations_per_input = transliteration(
                            mh, [input_text], count)
                        result_arr.append(input_text[0])
                    else:
                        result_arr.append(input_text)
                result = du.join_by_probs(result_arr, num_translations_per_input)[
                    :num_translations_per_input]
            except Exception as e:
                raise error_handler.ServerError(operation='Single_Split_Korean', start_time=start_time, error=str(e),
                                                extra={'input_text': pure_input_text})

        extra = {
            'input_text': pure_input_text,
            'output': result,
            'count': num_translations_per_input
        }
        res, code = response_standard(
            start_time, operation='Single_Split_Korean', extra=extra)
        res['input_text'] = extra['input_text']
        res['output'] = extra['output']
        res['count'] = extra['count']
        return res, code


# 191202
# @api.doc(parser=parser)
# class SinglePlain(Resource):
#   #@jwt_token_required
#   def post(self, **kwargs):
#     start_time = time.perf_counter()
#     token = kwargs['decoded_token']
#     if 'dokum' in token['scope']:
#       args = parser.parse_args()
#
#       input_text = args['input_text']
#       count = args['count']
#       preprocess = args['preprocess']
#
#       pure_input_text = input_text
#       num_translations_per_input = count
#       result = None
#
#       if preprocess:
#         input_text = du.remove_special_symbol(input_text)
#         input_text = du.remove_not_support_char(input_text)
#
#       input_text = input_text.lower()
#       if rm_space(input_text) == '' or input_text == None:
#         raise error_handler.ValidationError(operation='Single_Plain', start_time=start_time,
#             message='input is Empty')
#       else:
#         try:
#           mh = get_model_instance()
#           result, num_translations_per_input = transliteration(mh, [input_text], count)
#         except Exception as e:
#           raise error_handler.ServerError(operation='Single_Plain', start_time=start_time, error=str(e))
#
#       extra = {
#         'input_text': pure_input_text,
#         'output': result[0],
#         'count': num_translations_per_input
#       }
#       res, code =  response_standard(start_time, operation='Single_Plain', extra=extra)
#       res['input_text'] = extra['input_text']
#       res['output'] = extra['output']
#       res['count'] = extra['count']
#       return res, code
#
#     else:
#       raise error_handler.NotAuthorizedError(operation='Single_Plain', start_time=start_time)
#
# @api.doc(parser=parser)
# class SingleSplitKorean(Resource):
#   #@jwt_token_required
#   def post(self, **kwargs):
#     start_time = time.perf_counter()
#     token = kwargs['decoded_token']
#     if 'dokum' in token['scope']:
#       args = parser.parse_args()
#
#       input_text = args['input_text']
#       count = args['count']
#       preprocess = args['preprocess']
#
#       pure_input_text = input_text
#       num_translations_per_input = count
#       result = None
#
#       if preprocess:
#         input_text = du.remove_special_symbol(input_text)
#         input_text = du.remove_not_support_char(input_text)
#
#       input_text = input_text.lower()
#       if rm_space(input_text) == '' or input_text == None:
#         raise error_handler.ValidationError(operation='Single_Split_Korean', start_time=start_time,
#             message='input is Empty')
#       else:
#         input_text_arr = du.detach_kor(input_text)
#         if len(input_text_arr) > 5:
#           raise error_handler.ValidationError(operation='Single_Split_Korean', start_time=start_time,
#             message='It takes too long to calculate the Probability, ( Seperate counts : %d )' % len(input_text_arr),
#             extra={'input_text': pure_input_text})
#
#         try:
#           mh = get_model_instance()
#           result_arr = list()
#           for item in input_text_arr:
#             input_text = item['text']
#             if item['language'] == 'en':
#               input_text, num_translations_per_input = transliteration(mh, [input_text], count)
#               result_arr.append(input_text[0])
#             else:
#               result_arr.append(input_text)
#           result = du.join_by_probs(result_arr, num_translations_per_input)[:num_translations_per_input]
#         except Exception as e:
#           raise error_handler.ServerError(operation='Single_Split_Korean', start_time=start_time, error=str(e),
#             extra={'input_text': pure_input_text})
#
#       extra = {
#         'input_text': pure_input_text,
#         'output': result,
#         'count': num_translations_per_input
#       }
#       res, code =  response_standard(start_time, operation='Single_Split_Korean', extra=extra)
#       res['input_text'] = extra['input_text']
#       res['output'] = extra['output']
#       res['count'] = extra['count']
#       return res, code
#
#     else:
#       raise error_handler.NotAuthorizedError(operation='Single_Split_Korean', start_time=start_time)
