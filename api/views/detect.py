import time
from flask_restplus import Resource, Namespace
from googletrans import Translator

from auth.jwt_token import jwt_token_required
from exceptions import error_handler
from utils.output_utils import response_standard
from utils.cleaning_utils import rm_space


api = Namespace('Detect', description='Language Detection API')

parser = api.parser()
# parser.add_argument('Authorization', help='Auth Key', location='headers',
#                    required=True)
parser.add_argument('input_text', type=str, help='Input a sentence',
                    required=True)


@api.doc(parser=parser)
class Detect(Resource):
    # @jwt_token_required
    def post(self, **kwargs):
        start_time = time.perf_counter()
        token = kwargs['decoded_token']
        if 'detect' in token['scope']:
            args = parser.parse_args()
            input_text = args['input_text']
            if rm_space(input_text) == '' or input_text == None:
                raise error_handler.ValidationError(operation='Detect_Language', start_time=start_time,
                                                    message='input is Empty')
            else:
                try:
                    translator = Translator()
                    output = dict()
                    detected = translator.detect(input_text)
                    output['lang'] = detected.lang
                    output['probs'] = detected.confidence
                except Exception as e:
                    raise error_handler.ServerError(
                        operation='Detect_Language', start_time=start_time, error=str(e))
                extra = {
                    'input_text': input_text,
                    'output': output
                }
                res, code = response_standard(
                    start_time, operation='Detect_Language', extra=extra)
                res['input_text'] = extra['input_text']
                res['output'] = extra['output']
                return res, code
        else:
            raise error_handler.NotAuthorizedError(
                operation='Detect_Language', start_time=start_time)
