import re
import json


def text_sub(text, regex):
    return re.sub(regex, '', text)


def decode2utf8(string):
    return string.decode('utf-8')


def json_load(string):
    return json.loads(string)


def replace(text, word, replace_word):
    word = word.strip()
    return str(text).replace(word, replace_word)


def rm_space(text):
    return text.replace(' ', '')


def rm_dbl_space(text):
    return ' '.join(text.split())
