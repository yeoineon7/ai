from logger.logger import MyLogger, LampLogger
from utils.common_utils import to_dict

import time

logger = MyLogger.instance()
lamp = LampLogger.instance()


def response_standard(start_time, code=200, operation='Response_Success', message='Response Success', extra=None):
    # logging
    log_response = to_dict(
        operation=operation,
        status_code=code,
        message=message,
        duration=time.perf_counter() - start_time,
        extra=extra
    )

    payload = extra or {}
    payload['status_code'] = code

    logger.printStandard(log_response)
    lamp.printLampStandard(
        operation=operation,
        desc=message,
        duration=log_response['duration'],
        payload=payload
    )

    return to_dict(status_code=code, message=message), code


def response_warning(start_time, code=400, error=None, operation='Validation_Error', message='Validation Error', extra=None):
    # logging
    log_response = to_dict(
        operation=operation,
        status_code=code,
        message=message,
        error=error,
        duration=time.perf_counter() - start_time,
        extra=extra
    )

    payload = extra or {}
    payload['status_code'] = code
    payload['error'] = error

    logger.printStandard(log_response)
    lamp.printLampStandard(
        operation=operation,
        desc=message,
        duration=log_response['duration'],
        payload=payload
    )

    return to_dict(status_code=code, message=message, error=error), code


def response_error(start_time, code=500, error=None, operation='Server_Error', message='Server Error', extra=None):
    # logging
    log_response = to_dict(
        operation=operation,
        status_code=code,
        message=message,
        error=error,
        duration=time.perf_counter() - start_time,
        extra=extra
    )

    payload = extra or {}
    payload['status_code'] = code
    payload['error'] = error

    logger.printError(log_response)
    lamp.printLampError(
        operation=operation,
        desc=message,
        duration=log_response['duration'],
        payload=payload
    )

    return to_dict(status_code=code, message=message, error=error), code
