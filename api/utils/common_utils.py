def to_dict(**kwargs):
    obj = dict()
    for k, v in kwargs.items():
        obj[k] = v
    return obj
