import re
from utils import cleaning_utils as cl_utils


def check_valid_text(text):
    '''
    text : 문자 입력
    output : 중국어 or 일본어 : False
             아니면 : True
    '''
    NOT_REGEX = re.compile(r'[^0-9a-zA-Z가-힣\W_]')
    # 숫자, 영어, 한글, 특수문자(-) 아닌것 찾아라 : 중국어, 일본어 잡다어 찾기
    # NOT_REGEX = re.compile(r'[^0-9a-zA-Z가-힣\W_]')
    if NOT_REGEX.findall(text) != []:
        return False
    # 숫자 or 영어 or 한글 or 특수문자(-)로만 이루어져 있는 것
    else:
        return True

# text = "$!@#나는AB나}|     A_Z             "
# text = 'こんにちは'
# text = '(한글) 中國天道蟲 (중국 무당벌레)'
# text = 'こん"$!@#나는AB나}|     A_Z             "にちは' # pass
#
# tt = check_valid_text(text)
# tt


'''
# 여기 수정 : 20200616
from api.utils import cleaning_utils as cl_utils
def remove_special_symbol(text):
  result = ''
  for c in text:
    if c.isalnum() or c.isspace() or c in ['&', '$', '#']:
      result += c
  return cl_utils.rm_dbl_space(result).strip()

def remove_not_support_char(text):
  regex = r'([^ㄱ-ㅎㅏ-ㅣa-zA-Z가-힣0-9\s]+)'
  return cl_utils.text_sub(text, regex)
'''

# from api.utils import cleaning_utils as cl_utils
# 여기 수정 : 20200101


def remove_special_symbol(text):
    result = ''
    for c in text:
        if c.isalnum() or c.isspace():
            result += c
    return cl_utils.rm_dbl_space(result).strip()


def replace_special_symbol_to_space(text):
    text = text.replace("(", " ")
    text = text.replace(".", " ")
    text = text.replace(",", " ")
    text = text.replace(")", " ")
    text = text.replace("_", " ")
    text = text.replace("-", " ")
    return text


def remove_not_support_char(text):
    regex = r'([^ㄱ-ㅎㅏ-ㅣa-zA-Z가-힣0-9\s|&]+)'
    text = replace_special_symbol_to_space(text)
    text = cl_utils.text_sub(text, regex)
    return cl_utils.rm_dbl_space(text).strip()


# text = "jwt_token"
# text = replace_special_symbol(text)
# text
# remove_not_support_char("jwt_token")


# test = "test is & test 한글(feat. 이랏) ,, () .. !"
# tt2 = remove_not_support_char(test)
# tt2
#
#
#
# def remove_special_symbol_v2(text):
#   result = ''
#   for c in text:
#     if c.isspace():
#       result += c
#   return cl_utils.rm_dbl_space(result).strip()


def detach_kor(text):

    match_arr = list()
    result = list()
    idx = 0
    flag = True  # 이전에 영어
    regex = r'(\s*[ㄱ-ㅎㅏ-ㅣ가-힣]+\s*)'
    matches = re.finditer(regex, text, re.MULTILINE)
    en_list = list()
    ko_list = list()
    for _, match in enumerate(matches):
        match_arr.append((match.start(), match.end()))

    for (start, end) in match_arr:
        # start와 end 사이만 한글

        if idx < start and flag is False:  # 이전에 한글이고 지금 영어
            if len(ko_list) != 0:
                result.append({'language': 'ko', 'text': ''.join(ko_list)})
                ko_list = list()

        while idx < start:  # 영어는 start 전까지
            flag = True
            en_list.append(text[idx])
            idx += 1

        if len(en_list) != 0:
            result.append({'language': 'en', 'text': ''.join(en_list)})
            en_list = list()

        if flag is False:  # 이전에 한글이고 지금 한글
            ko_list.append(text[start:end])
            idx = end
            flag = False
        else:  # 이전에 영어고 지금 한글
            ko_list = list()
            ko_list.append(text[start:end])
            idx = end
            flag = False

    if len(ko_list) != 0:
        result.append({'language': 'ko', 'text': ''.join(ko_list)})

    if idx != len(text):
        result.append({'language': 'en', 'text': text[idx:]})

    return result


def join_by_probs(text_arr, count):
    # 결과 배열 저장소
    result_arr = list()
    # 출력된 배열
    print("text_arr", text_arr)
    for items in text_arr:
        if type(items) is list:
            # 영문
            new_arr = list()
            if len(result_arr) == 0:
                # 원하는 갯수만
                result_arr = items[:count]
            else:
                # 새로운 확률 값들
                # 원하는 갯수만
                for new_item in items[:count]:
                    # 영문의 확률들
                    # 현재까지 split 된 독음들 + 현재 독음 합치기
                    for curr_item in result_arr:
                        new_obj = {
                            'probs': 0.,
                            'dokum': ''
                        }
                        new_obj['probs'] = curr_item['probs'] + \
                            new_item['probs']
                        new_obj['dokum'] = curr_item['dokum'] + \
                            new_item['dokum']
                        new_arr.append(new_obj)
                result_arr = new_arr
        else:
            # 한글
            if len(result_arr) == 0:
                result_arr.append({
                    'probs': 0.,
                    'dokum': items
                })
            else:
                for index, _ in enumerate(result_arr):
                    result_arr[index]['dokum'] += items
    new_list = sorted(result_arr, key=lambda k: k['probs'], reverse=True)

    return new_list
