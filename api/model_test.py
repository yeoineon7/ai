import sys
import time
import os

from flask import jsonify, Flask
from router.router import bp
from setting import config
from logger.logger import MyLogger, LampLogger
from nmt_custom.connector.api_connector import Connector
from utils import output_utils

# 20201112 add
sys.path.append(os.getcwd())
print(sys.path)

# Naming App
app = Flask(__name__)
app.register_blueprint(bp)

# Use Only CPU
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# model setter
Connector.instance()


@app.errorhandler(Exception)
def default_exception(err):
    start_time = time.perf_counter()
    if int(err.code) % 100 is 5:
        res, code = output_utils.response_error(
            operation='Server_Error',
            message='Server error',
            start_time=start_time,
            error=str(err),
            code=err.code
        )
    elif int(err.code) == 404:
        res, code = output_utils.response_standard(
            operation='Not_Found',
            message='Not found',
            start_time=start_time,
            code=err.code
        )
    else:
        res, code = output_utils.response_warning(
            operation='Default_exception',
            message='Default exception',
            start_time=start_time,
            error=str(err),
            code=err.code
        )
    return jsonify(res), code
