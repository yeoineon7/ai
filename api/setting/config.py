import socket
import os


def my_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
    except socket.error:
        return '127.0.0.1'
    return s.getsockname()[0]


# [DEFAULT]
BASE_DIR = '/ai/dokum'
DEBUG = False
HOST = '0.0.0.0'
PORT = 8000


# [SEVER INFO]
SERVER_NAME = socket.gethostname()
SERVER_IP = my_ip()


# [MODEL]
OUT_DIR = os.path.join(
    BASE_DIR, 'models/transbot_attention_model_vocab_0131/best_bleu')
INFER_MODE = 'beam_search'
SUBWORD_OPTION = 'spm'
NUM_GPUS = 0
BEAM_WIDTH = 10
NUM_TRANSLATIONS_PER_INPUT = 10

# [AUTH]
SECRET_KEY = 'AI!DATA_D1SCOVERY__'
SUBJECT = HOST + '/api/v1/auth'
ISSUER = 'KOOCCI'
SCOPE = ['dokum', 'detect']
AUTH_DAYS = 30

# [DATABASE]
DB_USER = 'yeo'
DB_NAME = 'DATA_BASE_NAME'
DB_PW = 'kt-add'
DB_HOST = HOST
DB_PORT = '5432'
DB_MIN_POOL = 1
DB_MAX_POOL = 10

# [LOGGING]
FILE_MAX_BYTES = 1024 * 1024 * 100  # 100MB
SAVE_DAYS = 365  # 1 Year
SAVE_COUNT = 5
LOG_FOLDER = 'log_files'
LOG_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '[%(asctime)s] %(levelname)s in %(message)s',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'formatter': 'default',
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stderr',
        },
        'custom': {
            'level': 'INFO',
            'formatter': 'default',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'log_files/dokum.log',
            'backupCount': SAVE_COUNT,
            'maxBytes': FILE_MAX_BYTES
        },
        'lamp': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'log_files/lamp.log',
            'backupCount': SAVE_COUNT,
            'maxBytes': FILE_MAX_BYTES
        }
    },
    'loggers': {
        'custom_logger': {
            'level': 'DEBUG',
            'handlers': ['console', 'custom'],
            'propagate': 'no'
        },
        'lamp': {
            'level': 'INFO',
            'handlers': ['lamp'],
            'propagate': 'no'
        }
    }
}
