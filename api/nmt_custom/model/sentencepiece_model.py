import sentencepiece as spm


def use_sentencepiece_model(hparams, input_texts):
    s = spm.SentencePieceProcessor()
    model_file = "%s.%s.model" % (hparams.spm_model_prefix, hparams.src)
    assert model_file

    s.Load(model_file)
    return [' '.join([x for x in s.EncodeAsPieces(input_text.lower())]) for input_text in input_texts]
