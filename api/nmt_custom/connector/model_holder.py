from nmt_custom.utils import misc_utils as utils
from nmt_custom.model import model_helper, gnmt_model, attention_model, nmt_model, sentencepiece_model
from nmt_custom.connector.api_connector import Connector

import tensorflow as tf
import numpy as np


def get_model(hparams, flags, num_workers, jobid, out_dir):
    # hparams.inference_indices = None
    ckpt = flags.ckpt
    if not ckpt:
        ckpt = tf.train.latest_checkpoint(out_dir)
    model_creator = get_model_creator(hparams)
    infer_model = model_helper.create_infer_model(model_creator, hparams, None)
    return infer_model, ckpt, hparams, num_workers


def get_model_creator(hparams):
    """Get the right model class depending on configuration."""
    if (hparams.encoder_type == "gnmt" or
            hparams.attention_architecture in ["gnmt", "gnmt_v2"]):
        model_creator = gnmt_model.GNMTModel
    elif hparams.attention_architecture == "standard":
        model_creator = attention_model.AttentionModel
    elif not hparams.attention:
        model_creator = nmt_model.Model
    else:
        raise ValueError("Unknown attention architecture %s" %
                         hparams.attention_architecture)
    return model_creator


class ModelHolder:
    def __init__(self, infer_model, ckpt_path, hparams, num_workers):
        self.infer_model = infer_model
        self.ckpt_path = ckpt_path
        self.hparams = hparams
        self.num_workers = num_workers
        self.sess = tf.Session(
            graph=self.infer_model.graph, config=utils.get_config_proto())
        with self.infer_model.graph.as_default():
            self.loaded_infer_model = model_helper.load_model(
                self.infer_model.model, self.ckpt_path, self.sess, "infer")

    # input_texts = list
    def read_data(self, input_texts):
        infer_data = sentencepiece_model.use_sentencepiece_model(
            self.hparams, input_texts)
        # print("infer_data")
        # print(infer_data)
        # 20200603 추가 부분
        # infer_data = ['▁ s t r i n g', '▁ t s e t']
        infer_data = [x[2:] for x in infer_data]
        # infer_data
        print("--------------infer_data-----------:", infer_data)
        with self.infer_model.graph.as_default():
            self.sess.run(
                self.infer_model.iterator.initializer,
                feed_dict={
                    self.infer_model.src_placeholder: infer_data,
                    self.infer_model.batch_size_placeholder: self.hparams.infer_batch_size
                })

    def decode(self, num_translations_per_input=None, decode=True):
        subword_option = self.hparams.subword_option
        beam_width = self.hparams.beam_width
        tgt_eos = self.hparams.eos

        num_sentences = 0

        # 출력 갯수 설정 가능
        if num_translations_per_input is None:
            num_translations_per_input = self.hparams.num_translations_per_input
        num_translations_per_input = min(
            num_translations_per_input, beam_width)
        result = []

        while True:
            try:
                nmt_outputs, _, probs = self.loaded_infer_model.decode(
                    self.sess)
                batch_size = nmt_outputs.shape[1]
                probs = probs.reshape(-1)
                num_sentences += batch_size

                for sent_id in range(batch_size):
                    curr_sent = []
                    for beam_id in range(num_translations_per_input):
                        translation = self._get_translation(
                            nmt_outputs[beam_id],
                            sent_id,
                            tgt_eos=tgt_eos,
                            subword_option=subword_option)
                        curr_sent.append({'dokum': (translation).decode(
                            "utf-8"), 'probs': float(probs[beam_id])})
                    result.append(curr_sent)
            except tf.errors.OutOfRangeError:
                break
        return result, num_translations_per_input

    def _get_translation(self, nmt_outputs, sent_id, tgt_eos, subword_option):
        if tgt_eos:
            tgt_eos = tgt_eos.encode("utf-8")

        output = nmt_outputs[sent_id, :].tolist()

        if tgt_eos and tgt_eos in output:
            output = output[:output.index(tgt_eos)]

        if subword_option == "bpe":  # BPE
            translation = utils.format_bpe_text(output)
        elif subword_option == "spm":  # SPM
            translation = utils.format_spm_text(output)
        else:
            translation = utils.format_text(output)

        return translation


def get_model_instance():
    infer_model, ckpt, hparams, num_workers = Connector.instance().get_conn()
    return ModelHolder(infer_model, ckpt, hparams, num_workers)


def transliteration(mh, input_texts, count):
    mh.read_data(input_texts)
    return mh.decode(num_translations_per_input=count)
