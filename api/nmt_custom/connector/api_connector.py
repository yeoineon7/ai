from setting import config
from common.singleton_instance import Singleton
from nmt_custom.hparams import hparams_handler
from nmt_custom.connector import model_holder

import tensorflow as tf
import argparse
import sys


class Connector(Singleton):
    def __init__(self):
        # set default hparams
        nmt_parser = argparse.ArgumentParser()
        hparams_handler.add_arguments(nmt_parser)
        hparams_handler.FLAGS, unparsed = nmt_parser.parse_known_args()
        print("!!!!!!!!!!!!!!!!!!!1hparams", hparams_handler.FLAGS)

        # set config hparams
        hparams_handler.FLAGS.out_dir = config.OUT_DIR
        hparams_handler.FLAGS.infer_mode = config.INFER_MODE
        hparams_handler.FLAGS.subword_option = config.SUBWORD_OPTION
        hparams_handler.FLAGS.beam_width = int(config.BEAM_WIDTH)
        hparams_handler.FLAGS.num_translations_per_input = int(
            config.NUM_TRANSLATIONS_PER_INPUT)

        default_hparams = hparams_handler.create_hparams(hparams_handler.FLAGS)
        print("default_hparmas sucess")

        hparams, flags, num_workers, jobid, out_dir = hparams_handler.set_hparams(
            hparams_handler.FLAGS, default_hparams)
        print("default_hparmas sucess 2")

        print(hparams, flags, num_workers, jobid, out_dir)
        self.infer_model, self.ckpt, self.hparams, self.num_workers = model_holder.get_model(
            hparams, flags, num_workers, jobid, out_dir)
        print("model loaded")

    def get_conn(self):
        return self.infer_model, self.ckpt, self.hparams, self.num_workers
