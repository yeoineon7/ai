from flask import current_app as app
from psycopg2 import pool
from contextlib import contextmanager
from setting import config

import psycopg2
import psycopg2.extras


try:
    db_pool = pool.SimpleConnectionPool(
        config.DB_MIN_POOL,
        config.DB_MAX_POOL,
        user=config.DB_USER,
        password=config.DB_PW,
        host=config.DB_HOST,
        port=config.DB_PORT,
        database=config.DB_NAME)

    if (db_pool):
        print('DB Pool Connected successfully')

except psycopg2.DatabaseError as e:
    print('DB Connection Error : ' + str(e))


@contextmanager
def get_conn():
    conn = None
    try:
        conn = db_pool.getconn()
        yield conn
    except Exception as e:
        if conn:
            conn.rollback()
            print('Pool`s Connection Error : ' + str(e))
        else:
            print('conn is None')
    else:
        conn.commit()
    finally:
        db_pool.putconn(conn)


@contextmanager
def get_cursor(commit=False):
    with get_conn() as conn:
        cursor = conn.cursor(
            cursor_factory=psycopg2.extras.RealDictCursor)
        try:
            yield cursor
            if commit:
                conn.commit()
        finally:
            cursor.close()
