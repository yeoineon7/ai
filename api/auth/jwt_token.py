from flask import request
from setting import config
from exceptions import error_handler

import jwt
import datetime
import time


def encode_jwt():
    SECRET_KEY = config.SECRET_KEY
    ISSUER = config.ISSUER
    SUBJECT = config.SUBJECT
    date_time_obj = datetime.datetime
    exp_time = date_time_obj.timestamp(
        date_time_obj.utcnow() + datetime.timedelta(days=config.AUTH_DAYS))
    scope = config.SCOPE
    payload = {
        'sub': SUBJECT,
        'iss': ISSUER,
        'exp': int(exp_time),
        'scope': scope
    }

    return jwt.encode(payload, SECRET_KEY, algorithm='HS256').decode('utf-8')


def decode_jwt(token):
    SECRET_KEY = config.SECRET_KEY
    return jwt.decode(token, SECRET_KEY, algorithm='HS256')


def jwt_token_required(f):
    start_time = time.perf_counter()

    def decorated_function(*args, **kwargs):
        if not 'Authorization' in request.headers:
            raise error_handler.NotAuthorizedError(
                message='token is not given', start_time=start_time)
        token = request.headers['Authorization']
        try:
            # if token == 'test_token':
            # 	decoded_token = decode_jwt(encode_jwt())
            # else:
            decoded_token = decode_jwt(token)
            date_time_obj = datetime.datetime
            current_time = date_time_obj.timestamp(date_time_obj.utcnow())
            if current_time > decoded_token['exp']:
                raise error_handler.NotAuthorizedError(
                    message='Expired token given', start_time=start_time)

        except:
            raise error_handler.NotAuthorizedError(
                message='Invalid token given', start_time=start_time)
        else:
            kwargs['decoded_token'] = decoded_token
            return f(*args, **kwargs)

    return decorated_function
