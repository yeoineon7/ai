class RootException(Exception):
    def __init__(self, operation='', code=400, message='', error=None):
        Exception.__init__(self)
        self.operation = operation
        self.code = code
        self.message = message
        self.error = error
        self.start_time = None
        self.extra = None


class DefaultError(RootException):
    def __init__(self, code, start_time, message='Default Error', error=None, operation='Default_Error', extra=None):
        RootException.__init__(self)
        self.code = code
        self.message = message
        self.error = error
        self.operation = operation
        self.start_time = start_time
        self.extra = extra


class NotFoundError(RootException):
    def __init__(self, start_time, message='Not found', operation='Not_Found', extra=None):
        RootException.__init__(self)
        self.code = 404
        self.message = message
        self.operation = operation
        self.start_time = start_time
        self.extra = extra


class NotAuthorizedError(RootException):
    def __init__(self, start_time, message='Unauthorized', operation='Not_Authorized', extra=None):
        RootException.__init__(self)
        self.code = 401
        self.message = message
        self.operation = operation
        self.start_time = start_time
        self.extra = extra


class ValidationError(RootException):
    def __init__(self, start_time, message='Invalid Data', error=None, operation='Validation_Error', extra=None):
        RootException.__init__(self)
        self.code = 400
        self.message = message
        self.error = error
        self.operation = operation
        self.start_time = start_time
        self.extra = extra

    def __str__(self):
        return self.message


class PermissionError(RootException):
    def __init__(self, start_time, message='Permission Denied', operation='Permission_Error', extra=None):
        RootException.__init__(self)
        self.code = 403
        self.message = message
        self.operation = operation
        self.start_time = start_time
        self.extra = extra


class ServerError(RootException):
    def __init__(self, start_time, message='Server Error', error=None, operation='Server_Error', extra=None):
        RootException.__init__(self)
        self.code = 500
        self.message = message
        self.error = error
        self.operation = operation
        self.start_time = start_time
        self.extra = extra
