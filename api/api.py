# -*- coding: utf-8 -*-
import sys
import time
import os

from flask import jsonify, Flask, make_response
from router.router import bp
from setting import config
from logger.logger import MyLogger, LampLogger
from nmt_custom.connector.api_connector import Connector
from utils import output_utils

# 20201112 add
sys.path.append(os.getcwd())
print(sys.path)

app = Flask(__name__)
app.register_blueprint(bp)

# Use Only CPU
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# model setter
Connector.instance()

# model get_model_instance
# model test
# input_before =['Let It Go', 'Tonarino Totoro', '나비야', '나에게 내린 편지', '봄을 찾기', 'ある日、頭からツノが生えた ', 'Turning Up', 'Kurenai', '9と4分の3番線で君を待つ', 'Nessun Dorma', 'Avengers/Portals', 'Swan Lake', 'Nocturne', 'You Raise Me Up', 'The Swan', 'Tritsch-Tratsch Polka, Op.214', 'Chopin : Nocturne In C Minor op. 48/1 ', 'River Flows in You', 'Moon River', 'Yesterday', 'Si Tu Vois Ma Mere', 'Black Gold', 'Dream Coast', 'Revival', 'Paradise', 'Heroes Tonight', 'ON MY WAY', 'Sexual', 'Taped Up Pieces', '너와 나의 그 계절', '다 그렇지 뭐', '그리움의 언덕', '내 마음의 사진', 'Dance Monkey', 'If The World Was Ending', 'Love Affair', 'Maniac', 'BOY', '혼자', 'Loveship', '버릇처럼 셋을 센다', '다 그렇지 뭐', '신촌에 왔어', '그리움의 언덕', '좋은 사랑이었다', '혼자', '내 마음의 사진', '사랑 이토록 어려운 말', 'Loveship', 'I Made It', 'Unpack Something', 'Consistent', 'Naughty by Nature', 'Lord Knows', 'What It Do, What It Be Like', "In Case I Don't", 'Hundred Dollar Bills', 'Whiskey and Push-Ups', 'Fauvette', 'I Want It That Way', 'St Marie Under Canon', 'Confrontation', 'La Fin Absolue Du Monde', 'Revolution', 'The Dark Light', 'W.M.D.', 'Where the Demons Guide Me', 'Demon', 'Art - Violence', 'Mirror Song', 'Losing is the New Winning', 'Phenomenon', 'La Fora', 'Outside', 'Svyata', 'Oiymda sen gana', 'Rejected Deception from Was', 'Untold Dismissiveness', 'Planet of Dry Pus', 'Discussions of Yellow Bugs', 'Dont Be Eating No Pork', 'Refuse Oppression as Living', 'Basis Towards the Ability Mind', 'Excuse Me Scraps Extended When ', 'JLG', 'Tierra a la Vista', 'Algun Dia', 'Fruit Salad', 'Tradicion y Contradiccion', 'La Escuela', 'Promisedemptymoney', 'Sectionsofplanets', 'Nesteggsbreed', 'Detailbentprepared', 'Mata al Monstruo', 'Ella Es el Diablo', 'Guru', 'Baselineroadssilence', 'Et sted med regnbuer', 'Siete', 'Dinamita', 'Animal', 'Huracan', 'Una puta mierda', 'Surfeando en una ola de asfalto', 'Nos fuimos a la mierda', 'Sexo Mutante', 'Platonik', 'Night Terrors', 'Dissonant Child', 'Cybernetic Seduction', 'Like That', 'All over My Body', 'Dicen Que Viene y Va', 'Pelotas', 'Cerquita de Ti', 'Fantasmas de la Sobriedad', 'Ojos Pardos', 'Luna Llena', 'Vida Suicida', 'Himno de la Union', 'Hoy No Quiero Verte', 'Siente la Noche', 'Como los Bichos', 'Guadalajara', 'Serenata Mexicana', 'Intermezzo', 'Las mananitas', 'Mexicano', 'La Bamba', 'Mexico lindo y querido', 'Hello Romance', 'Rain', 'R & Deep', 'Timing ', 'Monkey', 'Cactus Juice', 'Air Fresh', 'Bubble Gum ', 'Finire', 'New Prank', 'Voice Star', 'Ocean Pearl', 'Flow with Me', 'Andare Oltre', 'Citta Sommersa', 'Ogni parte di te', 'Se questo e un uomo', 'Equilibrio instabile', 'Prendi fiato', 'Storie di precaria follia', 'Non un fine', 'In Too Deep', 'Huapango', "The Fairy Queen, Z. 629, Aria 'O Let Me Weep'", "King Arthur, Z. 628, Act V. 'Fairest Isle (Venus)' ", "The Indian Queen, Z.630, Aria 'They Tell Us That You Mighty Powers Above'", 'Fellowship', 'Modern South of Spain', 'Distant Lights', 'THE Sun Awakes', "The Way I've Always Been", 'Lonely Sorrow', 'Finally Free', 'Under the Purple Sun', 'Good Times', 'FOR a Change', 'African Calling', 'New Life', 'Fiesta En La Alcazaba', 'Surfing Andalucia', 'Un nuevo dia', 'Anaqueles ocultos', 'Spanish Sadness', 'Un cuento del sur', 'Haciendas y cortijos', 'Hasta el amanecer', 'Land III', 'Santa and the Kids', 'Big Tom is Still the King', "I Hope You're Having Better Luck Than Me", 'Sara Jane', 'Maria', 'Two Broken Hearts', 'A Little Bit of Love', 'Getting Warmer', 'Sometimes When We Touch', 'Love Has Joined Us Together', 'Bring Me Sunshine', 'Islands in the Stream', 'Downtown', 'Daydream Believer', 'Flashback', 'Teach Your Children', '좋은 사랑이었다', '좋은 사랑이었다', '거친 세상에서 실패하거든', '아무것도', 'Wake up !', '행복해야해요', '행복해야해요', '너 몰래', '텅텅', '오늘이 더 가기 전에', '서글픈 불면증', 'LOVED', 'Befit', '4.a.loners', "Good To Y'all", 'When I Feel Love', 'Human Part / REALationship', 'A Bite', 'Siren', 'Like A Coup', 'Seemulation', '언젠가 우리 가끔은', '이별준비', '사랑하자', '나비', "Don't make me cry", '차라리 만나지 않았더라면', '언젠가 우리 가끔은', '이별준비', 'Tonight', 'Mblues', 'Lililarang', '가쓰오우동', 'All I Can Bring', 'Blessed Be Your Name', 'Glorious', 'Sovereign Lord', 'Pierced', 'You Are The King Of Glory', 'King Of Glory', 'Faithful One', "He's Always Been Faithful", 'Your Love O God', 'Before The Throne Of God Above', 'Welcome Holy Spirit', 'We Fall Down', 'Sing Alleluia', 'Here I Am', 'Lord Have Mercy', 'Our Great God ', 'Here I Am Once Again', 'Joining The Angels In Heaven', "I'm Giving You My Heart", 'You Have Shown Me', 'I Can Only Imagine', 'The Wonderful Cross', 'Your Love Is Amazing', 'Blessed', "I'm Trading My Sorrows", 'We Will Seek Your Face', 'There Is A Louder Shout To Come', 'I Adore', 'Beautiful One', 'You', 'Peuvent pas', 'Red Bandeau', 'Get Louder', 'Once', 'Best for You', 'Two Time Lover', 'Regrets', '런닝맨', '런닝맨', 'INSPIRATION', '나 자신을 죽이다', '나의 주님', '기도 제목', '중보 기도', '나의 가족', '사랑하는 이들을 위해', '주님과의 동행', '어느새 잠든 당신', '행복한 도시', '마약 배게', '수면 매직', '평온한 소리', '어떻게 잠드냐구요', '점점 느리게', '썩은 둥지', '커다란 액자', '벽화', '나무 장판', '오래된 오르간', '카페트', '화로', '소음 차단', '순찰자', '바다의 중심', '아름다운 밤', '마음을 한가운데로', '참치', '울릉도 해변', '구름 사탕', '로즈마리', '시나몬 꽃 가루', '사과잼', '딸기잼', '마카롱', '버터향 디퓨저', 'Masterpiece', '연말정산', '산', '무지 갱', 'LTL', 'Fly Away']
#
# input_texts = [input_text.lower() for input_text in input_before]
# print("---------------input_texts---------------")
# print(input_texts)
# len(input_texts)
# type(input_texts)
#
#
#
# input_before =['Let It Go', 'Tonarino Totoro', '나비야', '나에게 내린 편지', '봄을 찾기', 'Turning Up', 'Kurenai', 'Nessun Dorma', 'Avengers/Portals', 'Swan Lake', 'Nocturne', 'You Raise Me Up', 'The Swan', 'Tritsch-Tratsch Polka, Op.214', 'Chopin : Nocturne In C Minor op. 48/1 ', 'River Flows in You', 'Moon River', 'Yesterday', 'Si Tu Vois Ma Mere', 'Black Gold', 'Dream Coast', 'Revival', 'Paradise', 'Heroes Tonight', 'ON MY WAY', 'Sexual', 'Taped Up Pieces', '너와 나의 그 계절', '다 그렇지 뭐', '그리움의 언덕', '내 마음의 사진', 'Dance Monkey', 'If The World Was Ending', 'Love Affair', 'Maniac', 'BOY', '혼자', 'Loveship', '버릇처럼 셋을 센다', '다 그렇지 뭐', '신촌에 왔어', '그리움의 언덕', '좋은 사랑이었다', '혼자', '내 마음의 사진', '사랑 이토록 어려운 말', 'Loveship', 'I Made It', 'Unpack Something', 'Consistent', 'Naughty by Nature', 'Lord Knows', 'What It Do, What It Be Like', "In Case I Don't", 'Hundred Dollar Bills', 'Whiskey and Push-Ups', 'Fauvette', 'I Want It That Way', 'St Marie Under Canon', 'Confrontation', 'La Fin Absolue Du Monde', 'Revolution', 'The Dark Light', 'W.M.D.', 'Where the Demons Guide Me', 'Demon', 'Art - Violence', 'Mirror Song', 'Losing is the New Winning', 'Phenomenon', 'La Fora', 'Outside', 'Svyata', 'Oiymda sen gana', 'Rejected Deception from Was', 'Untold Dismissiveness', 'Planet of Dry Pus', 'Discussions of Yellow Bugs', 'Dont Be Eating No Pork', 'Refuse Oppression as Living', 'Basis Towards the Ability Mind', 'Excuse Me Scraps Extended When ', 'JLG', 'Tierra a la Vista', 'Algun Dia', 'Fruit Salad', 'Tradicion y Contradiccion', 'La Escuela', 'Promisedemptymoney', 'Sectionsofplanets', 'Nesteggsbreed', 'Detailbentprepared', 'Mata al Monstruo', 'Ella Es el Diablo', 'Guru', 'Baselineroadssilence', 'Et sted med regnbuer', 'Siete', 'Dinamita', 'Animal', 'Huracan', 'Una puta mierda', 'Surfeando en una ola de asfalto', 'Nos fuimos a la mierda', 'Sexo Mutante', 'Platonik', 'Night Terrors', 'Dissonant Child', 'Cybernetic Seduction', 'Like That', 'All over My Body', 'Dicen Que Viene y Va', 'Pelotas', 'Cerquita de Ti', 'Fantasmas de la Sobriedad', 'Ojos Pardos', 'Luna Llena', 'Vida Suicida', 'Himno de la Union', 'Hoy No Quiero Verte', 'Siente la Noche', 'Como los Bichos', 'Guadalajara', 'Serenata Mexicana', 'Intermezzo', 'Las mananitas', 'Mexicano', 'La Bamba', 'Mexico lindo y querido', 'Hello Romance', 'Rain', 'R & Deep', 'Timing ', 'Monkey', 'Cactus Juice', 'Air Fresh', 'Bubble Gum ', 'Finire', 'New Prank', 'Voice Star', 'Ocean Pearl', 'Flow with Me', 'Andare Oltre', 'Citta Sommersa', 'Ogni parte di te', 'Se questo e un uomo', 'Equilibrio instabile', 'Prendi fiato', 'Storie di precaria follia', 'Non un fine', 'In Too Deep', 'Huapango', "The Fairy Queen, Z. 629, Aria 'O Let Me Weep'", "King Arthur, Z. 628, Act V. 'Fairest Isle (Venus)' ", "The Indian Queen, Z.630, Aria 'They Tell Us That You Mighty Powers Above'", 'Fellowship', 'Modern South of Spain', 'Distant Lights', 'THE Sun Awakes', "The Way I've Always Been", 'Lonely Sorrow', 'Finally Free', 'Under the Purple Sun', 'Good Times', 'FOR a Change', 'African Calling', 'New Life', 'Fiesta En La Alcazaba', 'Surfing Andalucia', 'Un nuevo dia', 'Anaqueles ocultos', 'Spanish Sadness', 'Un cuento del sur', 'Haciendas y cortijos', 'Hasta el amanecer', 'Land III', 'Santa and the Kids', 'Big Tom is Still the King', "I Hope You're Having Better Luck Than Me", 'Sara Jane', 'Maria', 'Two Broken Hearts', 'A Little Bit of Love', 'Getting Warmer', 'Sometimes When We Touch', 'Love Has Joined Us Together', 'Bring Me Sunshine', 'Islands in the Stream', 'Downtown', 'Daydream Believer', 'Flashback', 'Teach Your Children', '좋은 사랑이었다', '좋은 사랑이었다', '거친 세상에서 실패하거든', '아무것도', 'Wake up !', '행복해야해요', '행복해야해요', '너 몰래', '텅텅', '오늘이 더 가기 전에', '서글픈 불면증', 'LOVED', 'Befit', '4.a.loners', "Good To Y'all", 'When I Feel Love', 'Human Part / REALationship', 'A Bite', 'Siren', 'Like A Coup', 'Seemulation', '언젠가 우리 가끔은', '이별준비', '사랑하자', '나비', "Don't make me cry", '차라리 만나지 않았더라면', '언젠가 우리 가끔은', '이별준비', 'Tonight', 'Mblues', 'Lililarang', '가쓰오우동', 'All I Can Bring', 'Blessed Be Your Name', 'Glorious', 'Sovereign Lord', 'Pierced', 'You Are The King Of Glory', 'King Of Glory', 'Faithful One', "He's Always Been Faithful", 'Your Love O God', 'Before The Throne Of God Above', 'Welcome Holy Spirit', 'We Fall Down', 'Sing Alleluia', 'Here I Am', 'Lord Have Mercy', 'Our Great God ', 'Here I Am Once Again', 'Joining The Angels In Heaven', "I'm Giving You My Heart", 'You Have Shown Me', 'I Can Only Imagine', 'The Wonderful Cross', 'Your Love Is Amazing', 'Blessed', "I'm Trading My Sorrows", 'We Will Seek Your Face', 'There Is A Louder Shout To Come', 'I Adore', 'Beautiful One', 'You', 'Peuvent pas', 'Red Bandeau', 'Get Louder', 'Once', 'Best for You', 'Two Time Lover', 'Regrets', '런닝맨', '런닝맨', 'INSPIRATION', '나 자신을 죽이다', '나의 주님', '기도 제목', '중보 기도', '나의 가족', '사랑하는 이들을 위해', '주님과의 동행', '어느새 잠든 당신', '행복한 도시', '마약 배게', '수면 매직', '평온한 소리', '어떻게 잠드냐구요', '점점 느리게', '썩은 둥지', '커다란 액자', '벽화', '나무 장판', '오래된 오르간', '카페트', '화로', '소음 차단', '순찰자', '바다의 중심', '아름다운 밤', '마음을 한가운데로', '참치', '울릉도 해변', '구름 사탕', '로즈마리', '시나몬 꽃 가루', '사과잼', '딸기잼', '마카롱', '버터향 디퓨저', 'Masterpiece', '연말정산', '산', '무지 갱', 'LTL', 'Fly Away']
#
# from utils import dokum_utils as du
# input_texts = list(map(du.remove_special_symbol, input_before))
# input_texts
# input_texts = list(map(du.remove_not_support_char, input_texts))
# input_texts
# input_texts = [input_text.lower() for input_text in input_texts]
# input_texts
#
#
# from nmt_custom.connector.model_holder import get_model_instance, transliteration
# mh = get_model_instance()
# result, num_translations_per_input = transliteration(mh, input_texts, 2)
# result


@app.errorhandler(Exception)
def default_exception(err):
    start_time = time.perf_counter()
    if int(err.code) % 100 is 5:
        res, code = output_utils.response_error(
            operation='Server_Error',
            message='Server error',
            start_time=start_time,
            error=str(err),
            code=err.code
        )
    elif int(err.code) == 404:
        res, code = output_utils.response_standard(
            operation='Not_Found',
            message='Not found',
            start_time=start_time,
            code=err.code
        )
    else:
        res, code = output_utils.response_warning(
            operation='Default_exception',
            message='Default exception',
            start_time=start_time,
            error=str(err),
            code=err.code
        )
    return jsonify(res), code
