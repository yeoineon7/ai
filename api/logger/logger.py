from flask import request
from datetime import datetime
from setting import config
from common.singleton_instance import Singleton
from utils.uuid_utils import get_uuid

import logging
import logging.config
import socket
import json
import os
import sys


class LampLoggerAdapter(logging.LoggerAdapter):
    def __init__(self, logger):
        super(LampLoggerAdapter, self).__init__(logger, {})
        self.logger = logger
        self.host = config.SERVER_NAME
        self.ip = config.SERVER_IP
        self.caller = 'AI_DATA_Dokum'
        self.service = 'PG059005'

    def process(self, msg, kwargs):
        extra = kwargs['extra']
        timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        result = {
            'timestamp': timestamp,
            'host': {
                'name': self.host,
                'ip': self.ip
            },
            'caller': {
                'channel': self.caller
            },
            'service': self.service,
            'operation': extra['operation'],
            'transactionId': self.host + '_' + str(get_uuid()),
            'logType': extra['logType'],
            'response': {
                'type': extra['resType'],
                'code': extra['code']
            }
        }

        if extra['payload'] is not None:
            result['payload'] = extra['payload']

        if extra['duration'] is not None:
            result['response']['duration'] = int(
                round(extra['duration'] * 1000))
        if extra['desc'] is not None:
            result['response']['desc'] = extra['desc']

        return json.dumps(result, ensure_ascii=False), kwargs


class BaseLogger(Singleton):
    def __init__(self):
        # make log folder
        if not os.path.exists(config.LOG_FOLDER):
            sys.path.append(os.getcwd())
            os.mkdir(config.LOG_FOLDER)

        self.LOGCONFIG = config.LOG_CONFIG
        logging.config.dictConfig(self.LOGCONFIG)
        self.logger = None

    def get_logger(self):
        return self.logger


class MyLogger(BaseLogger):
    def __init__(self):
        print("My Logger Init")
        super().__init__()
        self.logger = logging.getLogger('custom_logger')

    def printError(self, payload):
        self.logger.error(json.dumps(payload, ensure_ascii=False))

    def printStandard(self, payload):
        self.logger.info(json.dumps(payload, ensure_ascii=False))


class LampLogger(BaseLogger):
    def __init__(self):
        print("Lamp Logger Init")
        super().__init__()
        self.logger = logging.getLogger('lamp')
        self.logger = LampLoggerAdapter(self.logger)

    def printLampError(self, operation, desc, logType='NOTIFY', duration=None, payload=None):
        resType = 'S'
        code = '999'
        self.logger.error(
            None,
            extra={
                'operation': operation,
                'logType': logType,
                'resType': resType,
                'desc': desc,
                'code': code,
                'duration': duration,
                'payload': payload
            }
        )

    def printLampStandard(self, operation, desc=None, logType='IN_MSG', duration=None, payload=None):
        resType = 'I'
        code = '200'
        self.logger.info(
            None,
            extra={
                'operation': operation,
                'logType': logType,
                'resType': resType,
                'desc': desc,
                'code': code,
                'duration': duration,
                'payload': payload
            }
        )
