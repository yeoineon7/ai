#!/bin/sh
#APP_USER=dokum
APP_USER=yeo
APP_NAME=uwsgi
CHK_PORT=8000
APP_LOC=$PWD
PID_NUM=`ps -ef | grep $APP_NAME | grep -v grep | wc -l`


UNAME=`id -u -n`
if [ e$UNAME != "e$APP_USER" ]
then
    echo "please select $APP_USER USER to start $APP_NAME...."
    exit;
fi

function start() {
    if [ $PID_NUM != 0 ]
    then
        echo "$APP_NAME is already running..."
        exit 0
    else
        nohup uwsgi --ini "$APP_LOC/wsgi.ini" 1> /dev/null 2>&1 &
        # nohup uwsgi --ini $APP_LOC/wsgi.ini >> $APP_LOC/log_files/nohup.log 2>&1 &
        echo "$APP_NAME started..."
        return 0
    fi
}

function stop() {
    if [ $PID_NUM != 0 ]
    then
        while [ $PID_NUM != 0 ]
            do
                killall -9 $APP_NAME
                PID_NUM=`ps -ef | grep $APP_NAME | grep -v grep | wc -l`
                sleep 3
            done
        echo "$APP_NAME stopped..."
        return 0
    else
       echo "$APP_NAME is not running..."
       exit 0
    fi
}

function status() {
    CHK_NET=`netstat -an | grep $CHK_PORT | grep LISTEN | wc -l`
    if [ $CHK_NET -lt 1 ]
    then
        echo "port $CHK_PORT is not listening"
        exit 3
    elif [ e$PID != "e" ]
    then
        echo "$APP_NAME process is not running!"
        exit 3
    else
        echo "$APP_NAME process is running OK at port $CHK_PORT!!"
        return 0
    fi

}

case "$1" in
    "start")
        echo "Starting UWSGI..."
        start
        # tail -f $APP_LOC/log_files/nohup.log
        sleep 5
        ;;
    "stop")
        echo "Stopping UWSGI..."
        stop
        sleep 5
        ;;
    "status")
        status
        ;;
    "restart")
        echo "Restarting UWSGI..."
        stop
        sleep 5
        start
        sleep 5
        ;;
    *)
        echo "Usage : `basename $0` {start | stop | status | restart}"
        exit 0
        ;;
esac
