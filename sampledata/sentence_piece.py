'''
updated: 2018.11.08

custom file to make sentencepiece model using setencepiece open source.
'''

import os
import sentencepiece as spm
import configparser
import config as config


class SentencePieceModel:
    def __init__(self, hparams):
        self.hparams = hparams

    def make_lowercase(self, src_file, lower_file):
        with open(src_file, 'r') as f1:
            with open(lower_file, 'w') as f2:
                for line in f1:
                    line = line.lower()
                    f2.write(line)
        return

    def make_sentencepiece_model(self, input_file, model_prefix):
        parameter = "--input=%s --model_prefix=%s --model_type=char \
    --character_coverage=1.0 --vocab_size=%d" % (input_file, model_prefix, self.hparams.sp_vocab_size)
        spm.SentencePieceTrainer_Train(parameter)

    # new train/dev/test file encoded by sentencepiece model
    def make_file_used_sentencepiece_model(self, src_file, tgt_file, model_file):
        s = spm.SentencePieceProcessor()
        s.Load(model_file)
        with open(src_file, 'r') as f1:
            with open(tgt_file, 'w') as f2:
                for line in f1:
                    # print(s.EncodeAsPieces(line))
                    if self.hparams.first_char_delete_flag == True:
                        line = ' '.join([str(x)
                                         for x in s.EncodeAsPieces(line)])
                        # print(line[1:])
                        f2.write(line[1:] + "\n")
                    else:
                        # line = ' '.join([x.decode("utf-8") for x in s.EncodeAsPieces(line)])
                        line = ' '.join([str(x)
                                         for x in s.EncodeAsPieces(line)])
                        # print(line)
                        f2.write(line + "\n")

    def create_and_use(self):
        # checking the sentencepiece model in arguments is exist.
        spm_used_train_src_file = "%s_spm_used.%s" % (
            self.hparams.train_prefix, self.hparams.src)
        spm_used_train_tgt_file = "%s_spm_used.%s" % (
            self.hparams.train_prefix, self.hparams.tgt)
        spm_used_dev_src_file = "%s_spm_used.%s" % (
            self.hparams.dev_prefix, self.hparams.src)
        spm_used_dev_tgt_file = "%s_spm_used.%s" % (
            self.hparams.dev_prefix, self.hparams.tgt)
        spm_used_test_src_file = "%s_spm_used.%s" % (
            self.hparams.test_prefix, self.hparams.src)
        spm_used_test_tgt_file = "%s_spm_used.%s" % (
            self.hparams.test_prefix, self.hparams.tgt)

        if self.hparams.sp_train_prefix is None:
            src_file = "%s.%s" % (self.hparams.train_prefix, self.hparams.src)
            tgt_file = "%s.%s" % (self.hparams.train_prefix, self.hparams.tgt)

            # make src_file/tgt_file data to lowercase
            src_lower_file = "%s.%s.lower" % (
                self.hparams.train_prefix, self.hparams.src)
            tgt_lower_file = "%s.%s.lower" % (
                self.hparams.train_prefix, self.hparams.tgt)

            self.make_lowercase(src_file, src_lower_file)
            self.make_lowercase(tgt_file, tgt_lower_file)

            # make src_lower_file/tgt_lower_file data to sentencepiece model
            # model file name is (src_file + .model), so model_prefix is src_file
            self.make_sentencepiece_model(src_lower_file, src_file)
            self.make_sentencepiece_model(tgt_lower_file, tgt_file)

            src_model_file = "%s.model" % (src_file)
            tgt_model_file = "%s.model" % (tgt_file)

            # create new file about train data
            self.make_file_used_sentencepiece_model(
                src_lower_file, spm_used_train_src_file, src_model_file)
            self.make_file_used_sentencepiece_model(
                tgt_lower_file, spm_used_train_tgt_file, tgt_model_file)

        if self.hparams.sp_dev_prefix is None:
            src_file = "%s.%s" % (self.hparams.dev_prefix, self.hparams.src)
            tgt_file = "%s.%s" % (self.hparams.dev_prefix, self.hparams.tgt)

            # make src_file/tgt_file data to lowercase
            src_lower_file = "%s.%s.lower" % (
                self.hparams.dev_prefix, self.hparams.src)
            tgt_lower_file = "%s.%s.lower" % (
                self.hparams.dev_prefix, self.hparams.tgt)

            self.make_lowercase(src_file, src_lower_file)
            self.make_lowercase(tgt_file, tgt_lower_file)

            # make src_lower_file/tgt_lower_file data to sentencepiece model
            # model file name is (src_file + .model), so model_prefix is src_file
            self.make_sentencepiece_model(src_lower_file, src_file)
            self.make_sentencepiece_model(tgt_lower_file, tgt_file)

            src_model_file = "%s.model" % (src_file)
            tgt_model_file = "%s.model" % (tgt_file)

            # create new file about dev data
            self.make_file_used_sentencepiece_model(
                src_lower_file, spm_used_dev_src_file, src_model_file)
            self.make_file_used_sentencepiece_model(
                tgt_lower_file, spm_used_dev_tgt_file, tgt_model_file)

        if self.hparams.sp_test_prefix is None:
            src_file = "%s.%s" % (self.hparams.test_prefix, self.hparams.src)
            tgt_file = "%s.%s" % (self.hparams.test_prefix, self.hparams.tgt)

            # make src_file/tgt_file data to lowercase
            src_lower_file = "%s.%s.lower" % (
                self.hparams.test_prefix, self.hparams.src)
            tgt_lower_file = "%s.%s.lower" % (
                self.hparams.test_prefix, self.hparams.tgt)

            self.make_lowercase(src_file, src_lower_file)
            self.make_lowercase(tgt_file, tgt_lower_file)

            # make src_lower_file/tgt_lower_file data to sentencepiece model
            # model file name is (src_file + .model), so model_prefix is src_file
            self.make_sentencepiece_model(src_lower_file, src_file)
            self.make_sentencepiece_model(tgt_lower_file, tgt_file)

            src_model_file = "%s.model" % (src_file)
            tgt_model_file = "%s.model" % (tgt_file)

            # create new file about test data
            self.make_file_used_sentencepiece_model(
                src_lower_file, spm_used_test_src_file, src_model_file)
            self.make_file_used_sentencepiece_model(
                tgt_lower_file, spm_used_test_tgt_file, tgt_model_file)

        return


class Hparams:
    def __init__(self, config):
        self.sp_train_prefix = config.sp_train_prefix
        self.sp_dev_prefix = config.sp_dev_prefix
        self.sp_test_prefix = config.sp_test_prefix
        self.sp_vocab_size = config.sp_vocab_size
        self.train_prefix = config.train_prefix
        self.test_prefix = config.test_prefix
        self.dev_prefix = config.dev_prefix
        self.src = config.src
        self.tgt = config.tgt
        self.first_char_delete_flag = config.first_char_delete_flag


if __name__ == "__main__" :
    print(config)
    hparams = Hparams(config=config)
    hparams.sp_vocab_size
    # line = ' '.join([x for x in ['▁', 't', 'v', '를', '▁', '껐', '네']])
    # type(line)
    spm_model = SentencePieceModel(hparams=hparams)
    spm_model.create_and_use()
