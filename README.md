# GNMT v2 For  Dokum Guide

구글 GNMT V2 Translation 모델을 커스터마이징 한 **독음기** 모델 학습 코드 & 독음기 API 코드

*This version of the tutorial requires [TensorFlow Nightly](https://github.com/tensorflow/tensorflow/#installation). For using the stable TensorFlow versions, please consider other branches such as [tf-1.4](https://github.com/tensorflow/nmt/tree/tf-1.4).*
*Python Version <u>3.6</u>*



# Table Of Contents

- [Introduction](https://gitlab.com/yeoineon7/ai#introduction)
  - [GNMT](https://gitlab.com/yeoineon7/ai#gnmt)
  - [Dokum](https://gitlab.com/yeoineon7/ai#dokum)
    - [Concept](https://gitlab.com/yeoineon7/ai#concept)
    - [Purpose](https://gitlab.com/yeoineon7/ai#purpose)
- [Setup](https://gitlab.com/yeoineon7/ai#setup)
  - [download Git Code](https://gitlab.com/yeoineon7/ai#download-git-code)
  - [Install Python Packages](https://gitlab.com/yeoineon7/ai#install-python-packages)
  - [Source code Architecture](https://gitlab.com/yeoineon7/ai#source-code-architecture)
- [Modeling](https://gitlab.com/yeoineon7/ai#Modeling)
  - [Model](https://gitlab.com/yeoineon7/ai#model)
    - [SPM(Sentencepiece Model)](https://gitlab.com/yeoineon7/ai#spm(sentencepiece-model))
    - [GNMT(Google Neural Machine Translation)](https://gitlab.com/yeoineon7/ai#gnmt(google-neural-machine-translation))
  - [Data](https://gitlab.com/yeoineon7/ai#datal)
    - [Data Type](https://gitlab.com/yeoineon7/ai#data-type)
    - [Prepare Data](https://gitlab.com/yeoineon7/ai#prepare-input(spm)-data)
    - [SPM Script](https://gitlab.com/yeoineon7/ai#spm-script)
  - [Train](https://gitlab.com/yeoineon7/ai#train)
    - [Train Script](https://gitlab.com/yeoineon7/ai#train-script)
    - [Argument](https://gitlab.com/yeoineon7/ai#argument)
  - [Blue Score](https://gitlab.com/yeoineon7/ai#blue-score)
    - [Best Model Check](https://gitlab.com/yeoineon7/ai#best-model-check)
- [API](https://gitlab.com/yeoineon7/ai#api)
  - [Setting](https://gitlab.com/yeoineon7/ai#api-setting)
    - [Uwsgi Setting](https://gitlab.com/yeoineon7/ai#uwsgi-setting)
    - [Python Setting](https://gitlab.com/yeoineon7/ai#python-setting)
  - [Run Server](https://gitlab.com/yeoineon7/ai#run-server)
    - [Uwsgi Run](https://gitlab.com/yeoineon7/ai#uwsgi-run)
    - [Flask Run](https://gitlab.com/yeoineon7/ai#flask-run)
  - [API Swagger Document](https://gitlab.com/yeoineon7/ai#api-swagger-document)
    - [API Type](https://gitlab.com/yeoineon7/ai#api-swagger-document)
    - [API Doc](https://gitlab.com/yeoineon7/ai#api-doc)

# Introduction



## GNMT

Google GNMT_v2 모델을 커스터마이징 함.
GNMT Github 공식 홈페이지의 [README.md](https://github.com/tensorflow/nmt)를 참고.
논문 : [Google's Neural Machine Translation System: Bridging the Gap between Human and Machine Translation](https://arxiv.org/abs/1609.08144) paper.



## Dokum

### Concept

- **독음**

  <u>독음 : 글을 읽는 소리(발음)</u>를 뜻하며 여기서는 **독음기**라 명명함.
  독음 모델 : 현재 `영어 --> 한글`을 위한 모델을 생성

- **예제**

> `Coldplay --> 콜드플레이`
>
> `slave to the parasites --> 슬레이브 투 더 패러사이트`
>
> `Viva La Vida --> 비바라비다`

### Purpose

- **Problem**

  기존 독음의 경우 사람이 하나하나 입력해야 하는 리소스가 발생
  
- **목적** 

  독음기의 목적 음성인식 기능의 향상을 위해 개발, 사람마다 다양한 종류의 영어발음을 가지고 있으며 커버리지의 영역을 높이기 위함





# Setup



## **Download Git Code**

```sh
git clone https://gitlab.com/yeoineon7/ai.git
```



## Install Python Packages

파이썬 버젼 *python3.6*이 우선 설치되어 있다고 가정한다.

**Create & Activate python3.6 **

```sh
python3 -m venv venv
source venv/bin/activate
```

**Install `requirements.txt` **

```sh
pip install --no-index requirements.txt
```



## Source Code  Architecture

```sh
dokum
├── api/ - api(Flask)를 위한 소스코드.
|   ├── auth/ - 권한 관리를 위한 디렉토리.
|   ├── common/ - 공통 코드
|   ├── db/ - 현재 사용하지 않음
|   ├── exceptions/ - 예외처리
|   ├── log_fiels/ - API 로그 관리 디렉토리.
|   ├── logger/ - logging 디렉토리
|   ├── nmt_custom/ - api내 nmt 모델 인스턴스 생성을 위한 디렉토리
|   ├── router/ - api routing을 위한 디렉토리
|   ├── setting/ - config 설정 관련 디렉토리
|   ├── utils/ - utils 관련 디렉토리
|   ├── view/ - api view 코드 관련 디렉토리
|   ├── api.py/ - api app생성 코드
|   ├── wsgi.ini/ - uwsgi config 설정 관련 파일
|   ├── wsgi.py/ - api 실행시 코드.
|   └── wsgi.sh/ - 현재 사용하진 않음
├── sampledata/ - Train, Test, Dev 입력 출력 1:1 데이터 셋.
|   ├── train.en/ - input 학습 셋
|   ├── train.fr/ - output 학습 셋
|   ├── test.en/ - input 테스트 셋
|   ├── test.fr/ - output 테스트 셋
|   ├── dev.en/ - input 평가 셋
|   └── dev.en/ - output 평가 셋
├── train/ - GNMT 독음기 학습을 위한 폴더 디렉토리.
├── models/ - 모델 생성 디렉토리 #mkdir로 models 디렉토리 생성
├── venv/ - python3 가상환경.
├── README.md/ - README 파일.
└── requirements.txt - requirements.txt 파일
```





# Modeling

## Model

크게 독음 모델을 생성하기 위해 <u>2가지</u> 모델을 사용함.

### SPM(Sentencepiece Model)

입력 데이터와 입력데이터의 사전관리를 위한 모델, `분절(subword)`단위로 단어를 쪼개지 않으면 사전이 무한대로 증가
현재는 `one length`기반으로 모두 쪼개어 사용`(test -> _t e s t_)`함.

> **SPM(Sentencepiece Model)**
> *BPE기반의 알고리즘, 분절(subword unit)처리기로 띄어쓰기 오류 및 미등록(Out Of Vocabulary) 대응*

### GNMT(Google Neural Machine Translation)

> **GNMT(Google Neural Machine Translation)**
> *혼합(mixed) 언어 처리(Stacked residual LSTM Using Attention Model)*



## Data

### Data Type

`/sampledata` 디렉토리에 셈플 데이터가 존재

**Input  & Output**

- `*.en ` : input 데이터를 뜻하며 <u>영어</u>
- `*.fr` : output 데이터를 뜻하며 <u>한글 발음</u>

**Data** 

`train(학습셋), test(테스트셋), dev(평가셋)`<u>3가지</u> 데이터가 존재



### Prepare  Data

`train(학습셋), test(테스트셋), dev(평가셋)`<u>3가지</u> 데이터 모두 spm모델을 이용하여 학습데이터를 만든다.

```sh
cd /ai/dokum/sampledata/
```

**spm config 변경**

```
sp_train_prefix = None
sp_dev_prefix =  None
sp_test_prefix = None
sp_vocab_size = 10000
train_prefix = '/ai/dokum/sampledata/train'
test_prefix = '/ai/dokum/sampledata/tst'
dev_prefix = '/ai/dokum/sampledata/dev'
src = 'en'
tgt = 'fr'
first_char_delete_flag = True
```

### SPM Script

```sh
python setence_piece.py
```

**Before Data -> SPM Data**

> skit radio 2부 호소문    --->    s k i t ▁ r a d i o ▁ 2 부 ▁ 호 소 문
>
> side by side            --->    s i d e ▁ b y ▁ s i d e



## Train

`train/nmt` 디렉토리로 이동 후 스크립트 실행

### Train Script

```sh
nohup python -m nmt \
  --num_translations_per_input=10 \
  --attention=scaled_luong \
  --attention_architecture=gnmt_v2 \
  --src=en \
  --tgt=fr \
  --vocab_prefix=/ai/dokum/sampledata/vocab \ #vocab.en, vocab.fr시 이름
  --share_vocab=True \ #공통 vocab사용
  --spm_model_prefix=/ai/dokum/sampledata/train \ #spm모델 이름 ex)train.model 일때 train
  --train_prefix=/ai/dokum/sampledata/train_spm_used \ #spm통과한 train data prefix
  --test_prefix=/ai/dokum/sampledata/tst_spm_used \ #spm통과한 test data prefix
  --dev_prefix=/ai/dokum/sampledata/dev_spm_used \ #spm통과한 dev data prefix
  --out_dir=/ai/dokum/models/transbot_gnmt_v2_20210811 \ #생성할 모델 명
  --optimizer=adam \
  --num_train_steps=350000 \
  --steps_per_stats=100 \
  --num_decoder_layers=4 \
  --num_encoder_layers=4 \
  --num_units=256 \
  --dropout=0.5 \
  --metrics=bleu \
  --beam_width=10 \
  --infer_mode=beam_search \
  --learning_rate=0.001 \
&

```

###  Argument

`/train/nmt`디렉토리에 `nmt.py`  학습 스크립트가 존재 하며 하기와 같은 옵션을 학습

- `--num_translations_per_input`: `inference`추론시 한번에 독음할 개수
- `--attention`: attention 종류,  luong | scaled_luong | bahdanau | normed_bahdanau 4가지 타입 존재
- `--attention_architecture`: attention 매카니즘
  - standard : use top layer to compute attention
  - gnmt: GNMT style of computing attention, use previous bottom layer to compute attention
  - gnmt_v2: similar to gnmt, but use current bottom layer to compute attention
- `--src`: input 데이터 끝말
- `--tgt`: target 데이터 끝말
- `--vocab_prefix` : vocab 사전 명
- `--share_vocab` : input, target 데이터의 사전 공유 여부
- `--spm_model_prefix` : `xxxx.model` spm 모델 명
- `--dev_prefix` : `dev` 데이터 명
- `--test_prefix` : `test` 데이터 명
- `--train_prefix` : `train` 데이터 명
- `--optimizer` : `sgd | adam`  2가지 존재
- `--num_train_steps` : training 횟수
- `--steps_per_stats` : `steps_per_stats`마다 로깅, 체크포인트는 10x `steps_per_stats`마다 저장
- `--num_decoder_layers` : Num decoder layer
- `--num_encoder_layers` : Num encoder layer
- `--num_units` : 네트워크 사이즈
- `--infer_mode`: `beam search`  사용시 다중 출력 가능





## Bleu Score

BLEU는 기계 번역 결과와 사람이 직접 번역한 결과가 얼마나 유사한지 비교하여 번역에 대한 성능을 측정하는 방법. 측정 기준은 n-gram에 기반.
언어에 구애받지 않고 사용할 수 있으며, 계산 속도가 빠릅니다. BLEU는 PPL과는 달리 높을 수록 성능이 더 좋음을 의미.

### Best Model Check

`/models/{out_model_name}/hparams` 베스트 모델 확인

```sh
vi /models/{out_model_name}/hparams
```

`best_bleu`점수와 `best_bleu_dir`

> "best_bleu": 80.39795907698955
>
> "best_bleu_dir": "/data/dokum/models/transbot_attention_model_vocab_0131/best_bleu",



# API

## Setting

### Uwsgi  Setting

 **uwsgi.ini 파일 열기**

```sh
vi uwsgi.ini
```

**2개의 디렉토리 환경에 맞게 설정**

`venv` : 파이썬 가상환경 디렉토리로 변경
`chdir` : api 폴더를 가르키는 디렉토리로 변경

```
[uwsgi]
protocol = http
socket = 0.0.0.0:8000

master = true
processes = 2

chdir = /ai/dokum/api

module = api
callable = app

virtualenv = /ai/dokum/venv

plugins = python3
```



### Python Setting

`config.py` : 폴더 디렉토리로 이동

```sh
vi /ai/dokum/api/setting/config.py
```

`BASE_DIR` :  dokum이 있는 디렉토리로 변경

```sh
# [DEFAULT]
BASE_DIR = '/ai/dokum'
DEBUG = False
HOST = '0.0.0.0'
PORT = 8000
```



## Run Server

### Uwsgi Run

`wsgi.ini` 존재하는 디렉토리로 이동

```sh
cd /ai/dokum/api
uwsgi --ini wsgi.ini
```



### Flask Run

`wsgi.py`존재하는 디렉토리로 이동

```sh
cd /ai/dokum/api
python wsgi.py
```





## API Swagger Document

서버 구동 후, 크롬 창에서 아래 url로 접근

```
http://localhost:8000/docs/
```



### API Type

**Single**

하나의 입력 데이터 받아서 독음 처리

**Bulk**

여러개의 입력 데이터를 리스트로 받아서 독음 처리

**Split/korean**

뒤 url주소에 split/korean이 붙으면 <u>한글은 입력으로 넣지 않는다</u>
한글의 경우 한글발음이 한글이기 때문에 따로 넣지 않았으며 입력데이터가 한글이 많지 않기때문에 충분히 학습이 되어있지 않음



### API Doc

| 독음 방식                         | input (JSON)                                                 | output (JSON)                                                | curl                                                         |
| --------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| /api/v1/dokum/single/split/korean | {<br/>  "input_text": "This is test input 한글 제외",<br/>  "count": 1,<br/>  "preprocess": true,<br/>  "flag_simulator": true<br/>} | {   "status_code": 200,   "message": "Response Success",   "input_text": "This is test input 한글 제외",   "output": [     "디스 이즈 테스트 인풋 한글 제외"   ] } | curl -X POST "http://localhost:8000/api/v1/dokum/single/split/korean" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"input_text\": \"This is test input 한글 제외\", \"count\": 1, \"preprocess\": true, \"flag_simulator\": true}" |

`input_text` : 독음 처리할 input 영어 문장

`count` : 독음 출력 개수, `beam_search=10`으로 탐색하여 여러개의 발음이 나올 수 있으나, 현재 데이터에 충분한 발음이 있지 않기에 `1`로 사용 함

`preprocess` : 특수문자, 한자 등의 처리를 위한 flag

`flag_simulator` :  현재 기가지니 시뮬레이터에 사용되고 있으며 사용 여부 flag







## **API  TEST IMAGE**

### Requests

![requests](https://gitlab.com/yeoineon7/ai/-/blob/master/img/requests.png)

### Response

![response](https://gitlab.com/yeoineon7/ai/-/blob/master/img/response.png)



